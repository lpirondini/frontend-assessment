'user_strict'
var Shuffle = window.Shuffle;
var element = document.querySelector('.my-shuffle');
var sizer = element.querySelector('#sizer-element');

var shuffleInstance = new Shuffle(element, {
  itemSelector: '.col-md-4.card-wrapper',
  sizer: sizer
});

var filterBtns = document.querySelectorAll('.filters-list .filter');

for (var i = 0; i < filterBtns.length; i++) {
    filterBtns[i].addEventListener('click', function(event) {
      for (var i = 0; i < filterBtns.length; i++) {
        filterBtns[i].classList.remove('active');
      }
      console.log(this.getAttribute('data-group'));

      this.classList.add('active');
       shuffleInstance.filter(this.getAttribute('data-group'));

     });
}
